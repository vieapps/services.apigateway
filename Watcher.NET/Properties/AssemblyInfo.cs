﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]

[assembly: AssemblyTitle("VIEApps NGX API Watcher")]
[assembly: AssemblyCompany("VIEApps.net")]
[assembly: AssemblyProduct("VIEApps NGX")]
[assembly: AssemblyCopyright("© 2021 VIEApps.net")]

[assembly: AssemblyVersion("10.5.2201.1")]
[assembly: AssemblyFileVersion("10.5.2201.1")]
[assembly: AssemblyInformationalVersion("10.5.2022.01.04@net48#let.it.be")]
