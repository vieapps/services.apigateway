﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]

[assembly: AssemblyTitle("VIEApps NGX API Gateway")]
[assembly: AssemblyCompany("VIEApps.net")]
[assembly: AssemblyProduct("VIEApps NGX")]
[assembly: AssemblyCopyright("© 2021 VIEApps.net")]

[assembly: AssemblyVersion("10.5.2205.6")]
[assembly: AssemblyFileVersion("10.5.2205.6")]
[assembly: AssemblyInformationalVersion("10.5.2022.05.19@net48#let.it.be")]
